package tyt.sqlite;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Merge two BigPlanetSQLite databases into one which can be used 
 * by the Android apps including 'Big Planet Tracks' and 'RMaps'.
 * 
 * @version 0.2
 * @author Tsai-Yeh Tung (tytung@gmail.com)
 */
public class BigPlanetSQLiteMerger {
	
	private Connection sourceConn = null;
	private Connection targetConn = null;
	private static int BatchSize = 10000;
	
	private Connection getConnection(String sqliteDB) throws ClassNotFoundException, SQLException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");
		String url = "jdbc:sqlite:"+sqliteDB;
		Connection conn = DriverManager.getConnection(url);
		return conn;
	}

	public void mergeDBs(File sourceSQLite, File targetSQLite) {
		long startTime = System.currentTimeMillis();
		int totalRecords = 0;
		try {
			sourceConn = getConnection(sourceSQLite.getName());
			targetConn = getConnection(targetSQLite.getName());
			targetConn.setAutoCommit(false);
			
			Statement stmt = sourceConn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT count(*) FROM tiles");
			if (rs.next()) {
				totalRecords = rs.getInt(1);
			}
			rs.close();
			System.out.println("Total "+ totalRecords +" map tiles in "+ sourceSQLite.getName());
			
			PreparedStatement sourcePrepStmt = sourceConn.prepareStatement("SELECT * FROM tiles LIMIT ?,?");
			PreparedStatement targetPrepStmt = targetConn.prepareStatement("INSERT or REPLACE INTO tiles (x,y,z,s,image) VALUES (?,?,?,?,?)");
			boolean next = false;
			int limitSize = BatchSize;
			int startIndex = 0;
			int lastRecordNo = limitSize;
			do {
				sourcePrepStmt.setInt(1, startIndex);
				sourcePrepStmt.setInt(2, limitSize);
				rs = sourcePrepStmt.executeQuery();
				while (rs.next()) {
					targetPrepStmt.setInt(1, rs.getInt(1));
					targetPrepStmt.setInt(2, rs.getInt(2));
					targetPrepStmt.setInt(3, rs.getInt(3));
					targetPrepStmt.setInt(4, rs.getInt(4));
					byte[] tileData = rs.getBytes(5);
					targetPrepStmt.setBytes(5, tileData);
					targetPrepStmt.addBatch();
				}
				rs.close();
				targetPrepStmt.executeBatch();
				targetConn.commit();
				targetPrepStmt.clearBatch();
				
				if (lastRecordNo < totalRecords) {
					next = true;
				} else {
					next = false;
					lastRecordNo = totalRecords;
				}
				System.out.println("adding map tiles "+(startIndex+1)+"~"+(lastRecordNo)+" of total "+totalRecords);
				startIndex += limitSize;
				lastRecordNo += limitSize;
			} while (next);
			
			// V2.0 added: Update the 'info' table used by RMaps
			Statement stat = targetConn.createStatement();
			stat.executeUpdate("CREATE TABLE IF NOT EXISTS info AS SELECT 99 AS minzoom, 0 AS maxzoom");
			stat.execute("DELETE FROM info;");
			int min = 0, max = 0;
			rs = stat.executeQuery("SELECT DISTINCT z FROM tiles ORDER BY z ASC LIMIT 1;");
			if (rs.next())
				min = rs.getInt(1);
			rs.close();
			rs = stat.executeQuery("SELECT DISTINCT z FROM tiles ORDER BY z DESC LIMIT 1;");
			if (rs.next())
				max = rs.getInt(1);
			rs.close();
			
			PreparedStatement prepStat = targetConn.prepareStatement("INSERT INTO info (minzoom, maxzoom) VALUES (?,?);");
			prepStat.setInt(1, min);
			prepStat.setInt(2, max);
			prepStat.execute();
			prepStat.close();
			targetConn.commit();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (sourceConn != null)
					sourceConn.close();
				if (targetConn != null)
					targetConn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		float diffTime = (float)(System.currentTimeMillis() - startTime) / 1000;
		System.out.println("Total "+ diffTime +" seconds to add/replace "+ totalRecords +" map tiles into "+ targetSQLite);
	}
	
	public static void printAbout() {
		System.out.println("======================================");
		System.out.println("=     BigPlanetSQLiteMerger v0.2     =");
		System.out.println("=  Powered by Dr. TYT's Android Map  =");
		System.out.println("=  http://android-map.blogspot.com/  =");
		System.out.println("======================================");
	}
	
	public static void printFileSize(File sourceSQLite, File targetSQLite) {
		System.out.println("--------------------------------------");
		System.out.println(" "+sourceSQLite.getName()+": "+sourceSQLite.length()/1024+" KB");
		System.out.println(" "+targetSQLite.getName()+": "+targetSQLite.length()/1024+" KB");
		System.out.println("--------------------------------------");
	}
	
	public static void main(String[] args) {
		File sourceSQLite = null;
		File targetSQLite = null;
		
		String usage = "Usage: "+BigPlanetSQLiteMerger.class.getSimpleName()+" sourceSQLiteDB targetSQLiteDB [BatchSize=10000]";
		
		if (args.length < 2) {
			printAbout();
			System.err.println(usage);
			return;
		}
		
		for (int i = 0; i < args.length; i++) {
			try {
				sourceSQLite = new File(args[i++]);
				targetSQLite = new File(args[i++]);
				if (!sourceSQLite.isFile() || !targetSQLite.isFile()) {
					printAbout();
					System.err.println(usage);
					System.err.println("Files "+sourceSQLite+" and/or "+targetSQLite+" not found.");
					return;
				}
				if (args.length >= 3) {
					try {
						BatchSize = Integer.parseInt(args[i++]);
					} catch (Exception e) {
						printAbout();
						System.err.println(usage);
						System.err.println("BatchSize must be an integer.");
						return;
					}
				}
				if (args.length == 4) {
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
		
		printAbout();
		printFileSize(sourceSQLite, targetSQLite);
		
		BigPlanetSQLiteMerger merger = new BigPlanetSQLiteMerger();
		merger.mergeDBs(sourceSQLite, targetSQLite);
		
		printFileSize(sourceSQLite, targetSQLite);
	}

}
