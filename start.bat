@Echo off
REM This file will start the BigPlanetSQLiteMerger with custom memory settings for
REM the JVM. With the below settings the heap size (Available memory for the application)
REM will range from 64 megabyte up to 1024 megabyte.

Set SOURCE=source.sqlitedb
Set TARGET=target.sqlitedb

java -Xms64M -Xmx1024M -jar BigPlanetSQLiteMerger.jar %SOURCE% %TARGET% 10000
pause
